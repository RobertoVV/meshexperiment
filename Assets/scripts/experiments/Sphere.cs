﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour 
{
	[Range(0,10)]public float radius = 10.0f;
	[Range(3,30)]public int tessellationLevels = 10;
	public MeshFilter meshfilter;

	public Mesh mesh;
	public bool shouldRotate = true;
	public Vector3[] vertexes;
	public int[] triangles;

	private int processedTessellation;
	private float processedRadius;


	void Start () 
	{
		//***********CIRCLE
		/*
		Vector3[] vertexes;
		int[] triangles;
		float angleXZ = (360.0f/tessellationSteps) * Mathf.Deg2Rad;//en radianes
		float SinAngleXZ;
		float CosAngleXZ;
		float SinE;
		float x,y,z;

		vertexes = new Vector3[tessellationSteps+1];
		vertexes[0] = Vector3.zero;

		for(int i = 0; i<tessellationSteps; i++)
		{
			SinAngleXZ = Mathf.Sin(i*angleXZ);	
			CosAngleXZ = Mathf.Cos(i*angleXZ);

			//y = 0 -> arcos(0) = 90 (puntos en el plano x,z)
			SinE = 1;//sin(90) = 1

			x = CosAngleXZ*SinE*radius;
			y = 0;
			z = SinAngleXZ*SinE*radius;

			//vertex 0 is the origin
			vertexes[i+1] = new Vector3(x,y,z);
		}


		triangles = new int[tessellationSteps*3];
		for(int i = 0; i < tessellationSteps; i++)
		{
			triangles[(i*3)] = 0;//Origin
			triangles[(i*3)+2] = i+1;

			//se cierra la mesh
			if(i+2 == tessellationSteps+1) 
			{
				triangles[(i*3)+1] = 1;
			}
			else
			{
				triangles[(i*3)+1] = i+2;
			}
		}
		*/
		//*****************************************


		generateSphereMesh();
		presentMesh();

		//Random mountains
		int r = Random.Range(5,8);
		Debug.Log("Mountains["+r+"]");
		for(int i = 0; i < r; i++)
		{
			//addMountain use the normals from the mesh so it neede to be called after the first presentMesh
			addMountainTo(randomSphereEdgePoint(),Random.Range(radius*0.15f,radius*0.25f),Random.Range(radius*0.1f,radius*0.4f));
		}
		presentMesh();
	}

	void Update()
	{
		//If any value was changed in editor
		if(radius != processedRadius || tessellationLevels != processedTessellation)
		{
			generateSphereMesh();
			presentMesh();

			//Adding some mountains
			int r = Random.Range(5,8);
			Debug.Log("Mountains["+r+"]");
			for(int i = 0; i < r; i++)
			{
				//addMountain use the normals from the mesh so it neede to be called after the first presentMesh
				addMountainTo(randomSphereEdgePoint(),Random.Range(radius*0.15f,radius*0.25f),Random.Range(radius*0.1f,radius*0.1f));
			}
			presentMesh();
		}	

		if(shouldRotate)
		{
			transform.Rotate(Vector3.up*(5*Time.deltaTime));
		}
	}

	private void generateSphereMesh()
	{
		processedTessellation = tessellationLevels;
		processedRadius = radius;

		//x = cos(angleXZ) sin(e) r
		//z = sin(angleXZ) sin(e) r
		//y = cos(e)r
		//e = arcos(y/r)

		float angleXZ = (360.0f/tessellationLevels) * Mathf.Deg2Rad;//en radianes
		float yStep = radius/tessellationLevels;
		float SinAngleXZ;
		float CosAngleXZ;
		float SinE;
		float x,y,z;

		int levels = (tessellationLevels*2-1);
		int lastTriangle = 0;

		vertexes = new Vector3[(levels*tessellationLevels)+2];
		triangles = new int[ (levels*tessellationLevels*2) *3];//2 triangulos por nivel

		int yLimit = tessellationLevels-1;
		int index = 0;
		float startAngle = 0;

		for(int j = 0; j < levels; j++ )
		{
			y = (j-yLimit)*yStep;

			//Angle to the y axis
			//e = arcos(y/r)
			SinE = Mathf.Sin(Mathf.Acos(y/radius));

			for(int i = 0; i<tessellationLevels; i++)
			{

				SinAngleXZ = Mathf.Sin((i*angleXZ)+startAngle);	
				CosAngleXZ = Mathf.Cos((i*angleXZ)+startAngle);

				x = CosAngleXZ*SinE*radius;
				z = SinAngleXZ*SinE*radius;


				vertexes[index] = new Vector3(x,y,z);


				if(j < levels-1)
				{
					//Indices para los triangulos
					if(j == 0)
					{
						//first level triangles case
						triangles[lastTriangle++]=(j*tessellationLevels)+(i);//same Y
						triangles[lastTriangle++]=(j*tessellationLevels)+(i-1 < 0 ? (tessellationLevels-1):i-1);//same Y
						triangles[lastTriangle++]=(j*tessellationLevels)+(i+tessellationLevels);//Y above
					}
					else if(j == levels-2)
					{
						//top level triangles case
						triangles[lastTriangle++]=(j*tessellationLevels)+(i);//same Y
						triangles[lastTriangle++]=(j*tessellationLevels)+(i+tessellationLevels);//Y above
						triangles[lastTriangle++]=(j*tessellationLevels)+(i+1 == tessellationLevels ? tessellationLevels:i+1+tessellationLevels);//Y above

					}
						
					if(j <= levels-3)
					{
						//body
						triangles[lastTriangle++]=(j*tessellationLevels)+(i);//same Y
						triangles[lastTriangle++]=(j*tessellationLevels)+(i+tessellationLevels);//Y above
						triangles[lastTriangle++]=(j*tessellationLevels)+(i+1 == tessellationLevels ? tessellationLevels*2:i+1+tessellationLevels*2);//Y above 2 levels
								 
						triangles[lastTriangle++]=(j*tessellationLevels)+(i);//same Y
						triangles[lastTriangle++]=(j*tessellationLevels)+(i+1 == tessellationLevels ? tessellationLevels*2:i+1+tessellationLevels*2);//Y above 2 levels
						triangles[lastTriangle++]=(j*tessellationLevels)+(i+1 == tessellationLevels ? tessellationLevels:i+1+tessellationLevels);//Y above
					}
				}

				index++;
			}

			startAngle -= angleXZ*0.5f;
		}

		//cola y punta de la esfera
		vertexes[vertexes.Length-2] = new Vector3(0,-radius,0);
		vertexes[vertexes.Length-1] = new Vector3(0,radius,0);
		//Triangulos de abajo y de arriba
		for(int i = 0; i<tessellationLevels; i++)
		{
			//bottom
			triangles[lastTriangle++]=vertexes.Length-2;
			triangles[lastTriangle++]=i-1 < 0 ? (tessellationLevels-1):i-1;
			triangles[lastTriangle++]=i;

			//top
			triangles[lastTriangle++]=vertexes.Length-1;
			triangles[lastTriangle++]=((tessellationLevels*2-2)*tessellationLevels) +i;
			triangles[lastTriangle++]=((tessellationLevels*2-2)*tessellationLevels) + (i-1 < 0 ? (tessellationLevels-1):i-1);
		}
	}

	public void addMountainTo(Vector3 mountainCenter, float mountainRadius, float height)
	{
		float sqrRadius = mountainRadius*mountainRadius;

		Vector3 averageNormal = Vector3.zero;//Normal of the mountain based on the vertexes inside
		float vertexDistance;

		//Mountain normal
		for(int i = 0; i < vertexes.Length; i++)
		{
			//Avoiding the square root
			vertexDistance = (vertexes[i] - mountainCenter).sqrMagnitude;

			if(vertexDistance >= sqrRadius)
			{
				continue;
			}

			//Distancia normalizada
			vertexDistance = Mathf.Sqrt(vertexDistance);
			//with a fallof so the closest vertexes apply more influence
			averageNormal = (1.1f - vertexDistance/mountainRadius)*mesh.normals[i];
		}

		//normalized so we only have the direction
		averageNormal = averageNormal.normalized;

		//Adding height to the involved vertexes
		for(int i = 0; i < vertexes.Length; i++)
		{
			//Avoiding the square root
			vertexDistance = (vertexes[i] - mountainCenter).sqrMagnitude;

			if(vertexDistance >= sqrRadius)
			{
				continue;
			}
				
			//Distancia normalizada
			vertexDistance = Mathf.Sqrt(vertexDistance);

			//with a fallof so the closest vertexes apply more influence
			vertexes[i] += (1.2f - vertexDistance/mountainRadius)*height*averageNormal;
		}
	}

	protected void presentMesh()
	{
		mesh = meshfilter.mesh;
		mesh.Clear();

		mesh.vertices = vertexes;
		//mesh.uv = uvs;
		mesh.triangles = triangles;

		mesh.RecalculateNormals();
	}

	protected int getLinearIndexFrom(int xIndex,int yIndex)
	{
		return xIndex+yIndex*tessellationLevels;
	}

	public Vector3 randomSphereEdgePoint()
	{
		//x = cos(angleXZ) sin(e) r
		//z = sin(angleXZ) sin(e) r
		//y = cos(e)r
		//e = arcos(y/r)

		float angleXZ = Random.Range(0.0f,360.0f)*Mathf.Deg2Rad;
		float e = Random.Range(0.0f,360.0f)*Mathf.Deg2Rad;

		float SinAngleXZ = Mathf.Sin(angleXZ);
		float CosAngleXZ = Mathf.Cos(angleXZ);
		float SinE = Mathf.Sin(e);
		float CosE = Mathf.Cos(e);

		return new Vector3(CosAngleXZ*SinE*radius, CosE*radius, SinAngleXZ*SinE*radius);
	}

	public struct Capsule2D
	{
		public Vector3 A;
		public Vector3 B;
		public float radius;
	}
	public Capsule2D random2DCapsule(float radius, float allowedDista)
	{
		Capsule2D result = new Capsule2D();


		return result;
	}
}
